package com.mtk.cct.simplecardboard;

import android.opengl.GLES20;
import android.opengl.Matrix;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

public class WorldFloor extends OGLProgram
{
    private static final int COORDS_PER_VERTEX = 3;

    // We keep the light always position just above the user.
    private static final float[] LIGHT_POS_IN_WORLD_SPACE = new float[] {0.0f, 2.0f, 0.0f, 1.0f};

    private final float[] lightPosInEyeSpace = new float[4];

    // uniforms & attributes
    public int u_Model;
    public int u_MVMatrix;
    public int u_MVP;
    public int u_InverseVP;
    public int u_LightPos;
    private int a_Position;
    private int a_Normal;
    private int a_Color;

    // buffers
    protected FloatBuffer b_vertices;
    protected FloatBuffer b_colors;
    protected FloatBuffer b_normals;
    private float[] model = new float[16];

    public WorldFloor()
    {
        vertexShaderSource      = R.raw.vertex;
        fragmentShaderSource    = R.raw.fragment;

        Matrix.setIdentityM(model, 0);
        Matrix.translateM(model, 0, 0, -20f, 0); // Floor appears below user.
    }

    public void initParams()
    {
        u_Model     = GLES20.glGetUniformLocation(program, "u_Model");
        u_MVMatrix  = GLES20.glGetUniformLocation(program, "u_MVMatrix");
        u_MVP       = GLES20.glGetUniformLocation(program, "u_MVP");
        u_InverseVP = GLES20.glGetUniformLocation(program, "u_InverseVP");

        u_LightPos  = GLES20.glGetUniformLocation(program, "u_LightPos");

        a_Position  = GLES20.glGetAttribLocation(program, "a_Position");
        a_Normal    = GLES20.glGetAttribLocation(program, "a_Normal");
        a_Color     = GLES20.glGetAttribLocation(program, "a_Color");

        GLES20.glEnableVertexAttribArray(a_Position);
        GLES20.glEnableVertexAttribArray(a_Normal);
        GLES20.glEnableVertexAttribArray(a_Color);

        OGLUtils.checkGLError("Floor program params");
    }

    public void initBuffers()
    {
        // make a floor
        ByteBuffer bbVertices = ByteBuffer.allocateDirect(FLOOR_COORDS.length * 4);
        bbVertices.order(ByteOrder.nativeOrder());
        b_vertices = bbVertices.asFloatBuffer();
        b_vertices.put(FLOOR_COORDS);
        b_vertices.position(0);

        ByteBuffer bbNormals = ByteBuffer.allocateDirect(FLOOR_NORMALS.length * 4);
        bbNormals.order(ByteOrder.nativeOrder());
        b_normals = bbNormals.asFloatBuffer();
        b_normals.put(FLOOR_NORMALS);
        b_normals.position(0);

        ByteBuffer bbColors = ByteBuffer.allocateDirect(FLOOR_COLORS.length * 4);
        bbColors.order(ByteOrder.nativeOrder());
        b_colors = bbColors.asFloatBuffer();
        b_colors.put(FLOOR_COLORS);
        b_colors.position(0);
    }

    /**
     * Draw the floor.
     *
     * <p>This feeds in data for the floor into the shader. Note that this doesn't feed in data about
     * position of the light, so if we rewrite our code to draw the floor first, the lighting might
     * look strange.
     */
    public void draw(float[] view, float[] projection)
    {
        GLES20.glUseProgram(program);

        // Set the position of the light
        Matrix.multiplyMV(lightPosInEyeSpace, 0, view, 0, LIGHT_POS_IN_WORLD_SPACE, 0);
        GLES20.glUniform3fv(u_LightPos, 1, lightPosInEyeSpace, 0);

        float[] modelViewProjection     = new float[16];
        float[] modelView               = new float[16];
        float[] viewProjection          = new float[16];
        float[] inverseViewProjection   = new float[16];

        Matrix.multiplyMM(viewProjection, 0, projection, 0, view, 0);
        Matrix.invertM(inverseViewProjection, 0, viewProjection, 0);
        GLES20.glUniformMatrix4fv(u_InverseVP, 1, false, inverseViewProjection, 0);

        // Set modelView for the floor, so we draw floor in the correct location
        Matrix.multiplyMM(modelView, 0, view, 0, model, 0);
        Matrix.multiplyMM(modelViewProjection, 0, projection, 0, modelView, 0);

        GLES20.glUniformMatrix4fv(u_Model, 1, false, model, 0);
        GLES20.glUniformMatrix4fv(u_MVMatrix, 1, false, modelView, 0);
        GLES20.glUniformMatrix4fv(u_MVP, 1, false, modelViewProjection, 0);

        // Set ModelView, MVP, position, normals, and color.
        GLES20.glVertexAttribPointer(a_Position, COORDS_PER_VERTEX, GLES20.GL_FLOAT, false, 0, b_vertices);
        GLES20.glVertexAttribPointer(a_Normal, 3, GLES20.GL_FLOAT, false, 0, b_normals);
        GLES20.glVertexAttribPointer(a_Color, 4, GLES20.GL_FLOAT, false, 0, b_colors);

        GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, 24);

        OGLUtils.checkGLError("drawing floor");
    }

    // The grid lines on the floor are rendered procedurally and large polygons cause floating point
    // precision problems on some architectures. So we split the floor into 4 quadrants.
    public static final float[] FLOOR_COORDS = new float[] {
            // +X, +Z quadrant
            200, 0, 0,
            0, 0, 0,
            0, 0, 200,
            200, 0, 0,
            0, 0, 200,
            200, 0, 200,

            // -X, +Z quadrant
            0, 0, 0,
            -200, 0, 0,
            -200, 0, 200,
            0, 0, 0,
            -200, 0, 200,
            0, 0, 200,

            // +X, -Z quadrant
            200, 0, -200,
            0, 0, -200,
            0, 0, 0,
            200, 0, -200,
            0, 0, 0,
            200, 0, 0,

            // -X, -Z quadrant
            0, 0, -200,
            -200, 0, -200,
            -200, 0, 0,
            0, 0, -200,
            -200, 0, 0,
            0, 0, 0,
    };

    public static final float[] FLOOR_NORMALS = new float[] {
            0.0f, 1.0f, 0.0f,
            0.0f, 1.0f, 0.0f,
            0.0f, 1.0f, 0.0f,
            0.0f, 1.0f, 0.0f,
            0.0f, 1.0f, 0.0f,
            0.0f, 1.0f, 0.0f,
            0.0f, 1.0f, 0.0f,
            0.0f, 1.0f, 0.0f,
            0.0f, 1.0f, 0.0f,
            0.0f, 1.0f, 0.0f,
            0.0f, 1.0f, 0.0f,
            0.0f, 1.0f, 0.0f,
            0.0f, 1.0f, 0.0f,
            0.0f, 1.0f, 0.0f,
            0.0f, 1.0f, 0.0f,
            0.0f, 1.0f, 0.0f,
            0.0f, 1.0f, 0.0f,
            0.0f, 1.0f, 0.0f,
            0.0f, 1.0f, 0.0f,
            0.0f, 1.0f, 0.0f,
            0.0f, 1.0f, 0.0f,
            0.0f, 1.0f, 0.0f,
            0.0f, 1.0f, 0.0f,
            0.0f, 1.0f, 0.0f,
    };

    public static final float[] FLOOR_COLORS = new float[] {
            0.0f, 0.3398f, 0.9023f, 1.0f,
            0.0f, 0.3398f, 0.9023f, 1.0f,
            0.0f, 0.3398f, 0.9023f, 1.0f,
            0.0f, 0.3398f, 0.9023f, 1.0f,
            0.0f, 0.3398f, 0.9023f, 1.0f,
            0.0f, 0.3398f, 0.9023f, 1.0f,
            0.0f, 0.3398f, 0.9023f, 1.0f,
            0.0f, 0.3398f, 0.9023f, 1.0f,
            0.0f, 0.3398f, 0.9023f, 1.0f,
            0.0f, 0.3398f, 0.9023f, 1.0f,
            0.0f, 0.3398f, 0.9023f, 1.0f,
            0.0f, 0.3398f, 0.9023f, 1.0f,
            0.0f, 0.3398f, 0.9023f, 1.0f,
            0.0f, 0.3398f, 0.9023f, 1.0f,
            0.0f, 0.3398f, 0.9023f, 1.0f,
            0.0f, 0.3398f, 0.9023f, 1.0f,
            0.0f, 0.3398f, 0.9023f, 1.0f,
            0.0f, 0.3398f, 0.9023f, 1.0f,
            0.0f, 0.3398f, 0.9023f, 1.0f,
            0.0f, 0.3398f, 0.9023f, 1.0f,
            0.0f, 0.3398f, 0.9023f, 1.0f,
            0.0f, 0.3398f, 0.9023f, 1.0f,
            0.0f, 0.3398f, 0.9023f, 1.0f,
            0.0f, 0.3398f, 0.9023f, 1.0f,
    };
}
