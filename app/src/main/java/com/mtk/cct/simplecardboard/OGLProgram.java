package com.mtk.cct.simplecardboard;

import android.opengl.GLES20;

public class OGLProgram
{
    protected int vertexShaderSource, fragmentShaderSource;

    protected int program;

    public void initProgram()
    {
        int vertexShader    = OGLUtils.loadGLShader(GLES20.GL_VERTEX_SHADER, vertexShaderSource);
        int fragmentShader  = OGLUtils.loadGLShader(GLES20.GL_FRAGMENT_SHADER, fragmentShaderSource);

        program = GLES20.glCreateProgram();
        GLES20.glAttachShader(program, vertexShader);
        GLES20.glAttachShader(program, fragmentShader);
        GLES20.glLinkProgram(program);
        GLES20.glUseProgram(program);

        OGLUtils.checkGLError("Floor program");
    }


}
