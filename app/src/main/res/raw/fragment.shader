precision mediump float;

uniform mat4 u_InverseVP;

varying vec4 v_Color;
varying vec3 v_Grid;
varying vec3 v_Pos;

void main()
{
    float depth = gl_FragCoord.z / gl_FragCoord.w; // Calculate world-space distance.

    // grid spans x-z direction in world space; check if on grid
    if ((mod(abs(v_Grid.x), 10.0) < 0.1) || (mod(abs(v_Grid.z), 10.0) < 0.1))
        gl_FragColor = // interpolate between two colors for grid lines
            max(0., 1.-depth/90.) * vec4(1.) /* white grid */
            + min(1., depth/90.) * v_Color;
    else
        gl_FragColor = v_Color;

    gl_FragColor = vec4(abs(v_Pos/100.), 1.);

    //gl_FragColor = u_InverseVP * vec4(gl_FragCoord.xy*2.0 - 1.0f, gl_FragCoord.z, 1);
}
